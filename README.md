# rofi-emoji

Extremely simple interface to using emojis, with rofi

---

## Installation

You should do some legwork before installing this script.

Once you're ready, `make install` should do the job for you.  If you want to install for a language other than English, say for Japanese, run `make lang=ja install`.

For a list of available languages, see [unicode-org/cldr](https://github.com/unicode-org/cldr/tree/master/common/annotations).

**Read the code.** It's less than 100 loc.

### Default installation configs

#### `Makefile`

```makefile
DEST=$(HOME)/.bin
ifdef $(XDG_DATA_HOME)
	DATA=$(XDG_DATA_HOME)/emoji
else
	DATA=$(HOME)/.data/emoji
endif
```

### Default Dependencies

* `awk`
* `curl`
* `xdotool`
* `xclip`

### Other notes

* You can use `make simpl-install` to just install `rofi-emoji` without downloading the entire emoji database. However, `rofi-emoji` still requires the database to function properly. This make command is good for making changes in the repo and installing the script.
* I highly recommend that users do their own installations and consider this repo as a template to installing their own script for rofi + emoji.

---

## Usage

```
rofi-emoji
```

#### Keybindings

* `Alt+u` to update emoji database (requires Internet connection)
