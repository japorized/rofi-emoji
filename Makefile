# rofi-emoji - rofi utility for emoji
.POSIX:

DEST=$(HOME)/.bin
ifdef $(XDG_DATA_HOME)
	DATA=$(XDG_DATA_HOME)/emoji
else
	DATA=$(HOME)/.data/emoji
endif

ifdef $(lang)
	lang=$(lang)
else
	lang=en
endif
EMOJIFILE=emoji.txt

prepare:
	chmod +x fetch-emoji.sh
	./fetch-emoji.sh -l $(lang)

uninstall:
	-rm -drf $(DATA)
	-rm $(DEST)/rofi-emoji

clean:
	-rm raw-emoji-list.txt emoji.txt
	-rm fetch-emoji-*

simpl-install:
	mkdir -p $(DATA)
	sed 's/lang="en"/lang="$(lang)"/g' ./fetch-emoji.sh > fetch-emoji-$(lang).sh
	chmod +x fetch-emoji-$(lang).sh
	cp fetch-emoji-$(lang).sh $(DATA)/fetch-emoji.sh
	cp rofi-emoji $(DEST)/
	chmod +x $(DEST)/rofi-emoji

install: prepare simpl-install
	cp $(EMOJIFILE) $(DATA)/$(EMOJIFILE)

clean-install: uninstall clean install
