#!/usr/bin/env bash

lang="en"

while getopts "l:" opt; do
  case "${opt}" in
    l) lang=$OPTARG ;;
    \?)
      usage
      exit 1
      ;;
  esac
done

url="https://raw.githubusercontent.com/unicode-org/cldr/master/common/annotations/${lang}.xml"

curl -s ${url} > ./raw-emoji-list.txt
exitstatus=$?

if [ $exitstatus = 0 ]; then
  sed -i '/annotation cp/!d' ./raw-emoji-list.txt

  if [ -f "emoji.txt" ]; then
    rm emoji.txt
  fi

  while read line; do
    icon=$(echo -n "${line}" | cut -d '>' -f1 | sed -E 's/type="tts"//g' | sed -E 's/.*cp=\"(.+?)\".*/\1/g' | sed -E 's/‍//g')
    desc=$(echo -n "${line}" | cut -d '>' -f2 | cut -d '<' -f1)
    echo "${icon} # ${desc}" >> emoji.txt
  done < ./raw-emoji-list.txt
else
  echo "curling failed"
  exit 1
fi
